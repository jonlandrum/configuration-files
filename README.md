Configuration Files
===============================================================================

This is a collection of configuration files I want to keep consistent across machines.

Changing the `motd` file
-------------------------------------------------------------------------------

* [Star Trek themed `motd`](etc/motd.sh "Run this script as root to change your motd")

Execute the above script as root or use `sudo`. If your system already has a `/etc/motd` file, you will have to back it up to `/etc/motd.bak` or delete it first, as the script looks for an existing file before writing anything. Furthermore, the script uses your system's hostname as the name it uses for the banner, so you will need to set the hostname to whichever _Star Trek_ character you wish to display on the banner, or otherwise manually change the `/etc/motd` file afterward. You will then have to source the resulting `/etc/motd` file from your `.bashrc` to see the banner.

This is a screenshot of the _Star Trek_ themed `motd` in use on my system:

![](etc/screenshot.png)
