# System-specific additions
# ---------------------------------------------------------
source ~/.zsh_additions

# Term setting
# ---------------------------------------------------------
export TERM='xterm-256color'

# History settings
# ---------------------------------------------------------
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000

# Enable Antigen plugin installer
# ---------------------------------------------------------
source ~/src/lib/antigen/antigen.zsh
antigen use oh-my-zsh
antigen theme agnoster/agnoster-zsh-theme
antigen bundle command-not-found
antigen bundle compleat
antigen bundle git
antigen bundle gitfast
antigen bundle git-extras
antigen bundle git-flow
antigen bundle git-remote-branch
antigen bundle sudo
antigen bundle thefuck
antigen bundle vi-mode
antigen bundle zsh-navigation-tools
antigen bundle zsh-users/zsh-completions
antigen bundle zsh-users/zsh-syntax-highlighting
antigen apply

# Oh-my-zsh settings
# ---------------------------------------------------------
COMPLETION_WAITING_DOTS="true"
HIST_STAMPS="yyyy-mm-dd"
ZSH_THEME=agnoster

# Configure Zsh Navigation Tools
# ---------------------------------------------------------
autoload znt-history-widget
zle -N znt-history-widget
bindkey "^R" znt-history-widget
zle -N znt-cd-widget
bindkey "^A" znt-cd-widget
zle -N znt-kill-widget
bindkey "^Y" znt-kill-widget

# Configure thefuck
# ---------------------------------------------------------
eval $(thefuck --alias)

# Compinit settings
# ---------------------------------------------------------
zstyle :compinstall filename '~/.zshrc'
zstyle ':completion:*' menu select
setopt COMPLETE_ALIASES
autoload -Uz compinit
compinit

# GPG TTY setting for signing Git commits
# ---------------------------------------------------------
export GPG_TTY=$(tty)

# Standardize listings
# ---------------------------------------------------------
alias ls="ls -hkv --color=always --group-directories-first"
if [ -x "`which dircolors`" -a -r "$HOME/.dir_colors" ]; then
    eval `dircolors -b "$HOME/.dir_colors"`
fi
alias l="ls -1"
alias la="ls -A"
alias ll="ls -l"
alias lla="ls -la"
alias lr="ls -R"
alias lrt="ls -lrt"

# Fix to make custom listings work with sudo
# ---------------------------------------------------------
alias sudo="sudo "

# Shortcuts
# ---------------------------------------------------------
alias ga="git add ."
alias gcs="git commit -vS"

# Vim key bindings
# ---------------------------------------------------------
bindkey -v
KEYTIMEOUT=5
reset-cursor() {
    printf '\033]50;CursorShape=1\x7'
}
export PS1="$(reset-cursor)$PS1"

# Prompt theming
# ---------------------------------------------------------
autoload -Uz promptinit
promptinit
