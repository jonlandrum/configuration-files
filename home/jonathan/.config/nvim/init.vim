"
" Initial setup and plugins
"

" Encoding and turn off filetype
scriptencoding utf-8
set nocompatible
filetype off

" Vim-plug
call plug#begin('~/.local/share/nvim/plugged')

" Auto Pairs
Plug 'jiangmiao/auto-pairs'
let g:AutoPairs={'(':')', '[':']', '{':'}',"'":"'",'"':'"', '`':'`'}

" Vim Airline
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
let g:airline_theme='solarized'
let g:airline_solarized_bg='dark'
let g:airline_powerline_fonts = 1

" git things
Plug 'tpope/vim-git'

Plug 'airblade/vim-gitgutter'
set updatetime=0

" tmux things
Plug 'tmux-plugins/vim-tmux'

Plug 'tmux-plugins/vim-tmux-focus-events'

" End Vim-plug
call plug#end()

" Turn filetype back on
filetype plugin indent on
syntax enable

"
" General Settings
"

" Put backup and swap files in a single directory
set backupdir=~/.config/nvim/tmp
set directory=~/.config/nvim/tmp
set clipboard+=unnamedplus

" Use an undo file to go back past last write
set undofile

" Set a directory to store the undo history
set undodir=~/.config/nvim/tmp

" Use Unix as the standard file type
set ffs=unix,dos,mac

" Set the cursor to stay roughly in the center of the screen
set so=30

" Turn on line numbers
set number

" Ignore compiled files
set wildignore=*.o,*~

" Show current position
set ruler

" Hide buffer when it is abandoned
set hid

" Configure backspace
set whichwrap+=<,>,h,l

" Set smart case searching
set smartcase

" For regular expressions, turn magic on
set magic

" Show matching brackets when text indicator is over them
set showmatch

" How many tenths of a second to blink when matching brackets
set mat=2


" 
" Colors
"

" Set a long line marker
set colorcolumn=80

" Enable syntax highlighting
syntax enable
colorscheme solarized
set background=dark
set termguicolors
let g:solarized_termtrans = 1
let g:solarized_termcolors=256
colorscheme solarized

" Show location
set cursorline
hi CursorLine ctermbg=4 ctermfg=0
hi Cursor ctermbg=0 ctermfg=4
set guicursor=n-v-c:block,i-ci-ve:ver25,r-cr:hor20,o:hor50,a:blinkwait700-blinkoff400-blinkon250-Cursor/lCursor,sm:block-blinkwait175-blinkoff150-blinkon175
au VimLeave * set guicursor=a:ver25-blinkon1

" 
" Text, tab, indent, etc
"

" Use spaces instead of tabs
set expandtab

" Use smart tabs
set smarttab
set shiftround

" 1 tab == 4 spaces
set shiftwidth=4
set tabstop=4
set softtabstop=4

" Linebreak on 100 characters
"set lbr
"set tw=100

" Smart indent
set si

" Wrap lines
set wrap

" 
" Line editing and spell checking
"

" Navigate with ] and [ when viewing diffs and merges
map ] ]c
map [ [c

" Pressing :ss will toggle and untoggle spell checking
map :ss :setlocal spell!<cr>

" Center the cursor on the screen when jumping
nnoremap n nzz
nnoremap } }zz

" Move by line instead of by row
nnoremap j gj
nnoremap k gk

" Map scroll wheel to move one line at a time
map <ScrollWheelUp> <C-Y>
map <S-ScrollWheelUp> <C-U>
map <ScrollWheelDown> <C-E>
map <S-ScrollWheelDown> <C-D>
