# Notes about items placed in `bin`

These scripts can either go in your local `~/bin` directory, or the global `/bin` directory. Directions for adding them to your `crontab` assume they are located in your local `~/bin` directory.

## cifsync

This program keeps your local work directory in sync with the remote CIFS share. Add it to your crontab to automate backups:

    0 0 * * 0 /home/username/bin/cifsync >/dev/null 2>&1

The script is written in such a way that it assumes you are also using the `poll-cifs` program found below, insofar that your remote share is mounted at `/mnt/share`, and that your remote work folder is `/mnt/share/work/current`. Designing it like this allows for "archiving" old projects into a separate folder beneath the work directory, such as `/mnt/share/work/archive`, which keeps your local drive cleaner while also maintaining old codebases.

### Dependencies

* `unison` ([information](https://www.cis.upenn.edu/~bcpierce/unison/))

## create-shell-logins

This is a script to automate the task of moving your SSH public key to the servers when you are granted access to a large group of them at once. You will need to do the following steps in order to use this script:

1. Change the servers in the `hosts` array to the machines you've been granted access to
2. Change the values of the `password` and `key` variables found in the `credentials.tcl` file
3. SCP or FTP your SSH public key to `/home/$USERNAME/id_rsa.pub` on the remote server

### Dependencies

* `/usr/bin/env`
* `expect` ([information](https://linux.die.net/man/1/expect))

## mounts

This is a script to automate mounting your Windows share. The name derives from "mount share". It continuously attempts to mount the share until the mount directory is populated. Thus, there are a couple of steps to take before you can successfully run the script:

1. Create a directory under `/mnt` where your share will initially be mounted. I chose `/mnt/corp` in the script, but you can call it whatever. Just be sure to change the script locally to reflect your modification.
2. Create a file called `cifs.credo` and store it somewhere in your user directory. I chose `/home/$USERNAME/etc/cifs.credo` in the script, but the naming is up to you. In this file, put `username=jsmith`, and `password=secret`, obviously changing these to match your username and password. If you put this file somewhere else, change the script accordingly.
3. Optionally, mount the share manually using the command in the script, and then create a symbollic link to your user directory located at `/mnt/corp/your/remote/directory`. I chose `/mnt/share` in the script for this symbollic link, but again, you can call it whatever you like, as long as you change the script accordingly.

If you change the name of the mount point or the path to the `cifs.credo` file, adjust line 10 of the script. If you opt not to have a symbollic link to your user directory, or if you choose to name it something other than `/mnt/share`, change lines 3 and 11 of the script.

The `cifs.credo` file above is formatted like a shell script, with new variables on new lines. There should be no whitespace around the `=` sign, and there should be no newline at the end of the file. You can also add a `domain` variable here, if you are having trouble getting the script to authenticate:

    username=jdoe
    password=secret
    domain=corp

### Dependencies

* `/usr/bin/env`
* `cifs-utils` ([information](https://wiki.samba.org/index.php/LinuxCIFS_utils))

## poll-cifs

This script pings your corporate domain server to test if the corporate network is available. If it is, it executes `mounts`, and if it is not, it executes `umounts`. Add the following to your crontab to poll continuously every minute:

    * * * * * /home/username/bin/poll-cifs > /dev/null 2>&1

### Dependencies

* `/usr/bin/env`
* `mounts`
* `umounts`

## umounts

This is a complementary script to `mounts` that automates unmounting the remote share. The name derives from "umount share". The only thing that might need to be changed in this script is lines 3, 7, and 8 if you've modified the naming of the mountpoint and symbollic link, as explained in the section above.

### Dependencies

* `/usr/bin/env`
* `cifs-utils` ([information](https://wiki.samba.org/index.php/LinuxCIFS_utils))
