Notes about items placed in `etc`
=================================

These files can either go in your root-level `/etc/` directory, or a user-level directory of your choosing. I chose the name "etc" so that it would make sense what these files are for.

Special instructions for using `motd.sh`
----------------------------------------

When your console shows the message of the day on login, it echoes the plaintext of the file `/etc/motd`. So dropping this script in its place won't do anything except print the source code. Instead, remove or back up the orginal `/etc/motd` file, then execute this script to replace it with the resulting script. On my machine, this is the resulting file:

```bash
#!/usr/bin/env bash

printf '^[[37m\n'
printf '                         .^.\n'
printf '                        /   \\\n'
printf '                       /     \\\n'
printf '               ^[[34m*******^[[37m/       \^[[34m*******\n'
printf '          ***** *****^[[37m/         \^[[34m***** *****\n'
printf '      ***** ********^[[37m/  ^[[33mBARCLAY^[[37m  \^[[34m******** *****\n'
printf '     *** **********^[[37m/             \^[[34m********** ***\n'
printf '      ***** ******^[[37m/               \^[[34m****** *****\n'
printf '          ***** *^[[37m/        _--_     \^[[34m* *****\n'
printf '               *^[[37m/      _-"^[[34m****^[[37m"\\    \^[[34m*\n'
printf '               ^[[37m/    _-" ^[[34m*****   ^[[37m"\\   \\\n'
printf '               \\__-"              "\\_/\n'
printf '\n'
printf '             \\\\//_ Live long and prosper\n'
printf '^[(B^[[m\n'
```

Next, add a system-wide call to this script so that it is executed on login. For example, I use ZSH, so I edited my system-wide `/etc/zshenv` file like so:

```bash
/bin/bash /etc/motd
```

This way, whether I shell into this machine remotely, log in directly, or `su` to another user, this `motd` is always displayed.
